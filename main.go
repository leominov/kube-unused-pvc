package main

import (
	"fmt"

	"github.com/dustin/go-humanize"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func main() {
	clientSet, err := NewClientSet()
	if err != nil {
		panic(err)
	}

	pvcCli := clientSet.CoreV1().PersistentVolumeClaims("")
	pvcList, err := getPVCInChunks(pvcCli, metav1.ListOptions{})
	if err != nil {
		panic(err)
	}

	podCli := clientSet.CoreV1().Pods("")
	for _, pvc := range pvcList.Items {
		pods, err := getPodsForPVC(podCli, pvc.Name, metav1.ListOptions{})
		if err != nil {
			fmt.Printf("Failed to get pods for %v pvc\n", pvc.Name)
			continue
		}
		if len(pods) == 0 {
			size := pvc.Spec.Resources.Requests.Storage()
			lifetime := humanize.Time(pvc.ObjectMeta.CreationTimestamp.Time)
			fmt.Printf("%s pvc/%s not used (%s) – %s\n", pvc.ObjectMeta.Namespace, pvc.Name, size.String(), lifetime)
		}
	}
}
