module gitlab.qleanlabs.ru/platform/infra/kube-unused-pvc

go 1.16

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/mailru/easyjson v0.7.7 // indirect
	k8s.io/api v0.22.0-beta.2
	k8s.io/apimachinery v0.22.0-beta.2
	k8s.io/cli-runtime v0.22.0-beta.2
	k8s.io/client-go v0.22.0-beta.2
)
